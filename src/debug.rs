use avian2d::debug_render::PhysicsDebugPlugin;
use bevy::prelude::*;
use iyes_perf_ui::{entries::PerfUiDefaultEntries, PerfUiPlugin};

use crate::{
    blast::SpawnBlast,
    character::movement::KeyboardControlled,
    game_state::InGame,
    levels::{spawn_box, Tiles},
    Despawn,
};

#[derive(Default)]
pub struct DebugPlugin;

#[derive(States, Clone, Hash, PartialEq, Eq, Debug, Default)]
enum DebugMode {
    #[default]
    Normal,
    Debug,
}

impl Plugin for DebugPlugin {
    fn build(&self, app: &mut App) {
        app.insert_state(DebugMode::Debug)
            .add_plugins((
                PerfUiPlugin::default(),
                bevy::diagnostic::FrameTimeDiagnosticsPlugin,
                bevy::diagnostic::EntityCountDiagnosticsPlugin,
                bevy::diagnostic::SystemInformationDiagnosticsPlugin,
                PhysicsDebugPlugin::default(),
            ))
            .add_systems(
                Update,
                (
                    spawn_box_on_key.run_if(resource_exists::<Tiles>),
                    blast_as_combined,
                )
                    .run_if(in_state(DebugMode::Debug))
                    .run_if(in_state(InGame)),
            )
            .add_systems(OnEnter(DebugMode::Debug), spawn_debug_ui)
            .add_systems(OnExit(DebugMode::Debug), despawn_debug_ui);
    }
}

fn spawn_box_on_key(keys: Res<ButtonInput<KeyCode>>, commands: Commands, tiles: Res<Tiles>) {
    if keys.just_pressed(KeyCode::KeyB) {
        spawn_box(commands, tiles, Vec2::new(-130.0, 100.0));
    }
}

fn blast_as_combined(
    transform: Query<&Transform, With<KeyboardControlled>>,
    keys: Res<ButtonInput<KeyCode>>,
    mut blast_spawner: EventWriter<SpawnBlast>,
) {
    let Ok(transform) = transform.get_single() else {
        return;
    };

    if keys.just_pressed(KeyCode::KeyP) {
        blast_spawner.send(SpawnBlast {
            location: transform.translation.truncate(),
        });
    }
}

#[derive(Component)]
struct DebugUi;

fn spawn_debug_ui(mut commands: Commands) {
    commands.spawn((DebugUi, PerfUiDefaultEntries::default()));
}

fn despawn_debug_ui(ui: Query<Entity, With<DebugUi>>, mut despawner: EventWriter<Despawn>) {
    for ui in ui.iter() {
        despawner.send(Despawn(ui));
    }
}
