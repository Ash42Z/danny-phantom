use avian2d::prelude::*;
use bevy::prelude::*;
use bevy_asset_loader::prelude::*;

use crate::character::visuals::CharacterTexture;
use crate::character::{CharacterType, SpawnCharacter};
use crate::game_state::{InGame, MainGameState};
use crate::npc::{Behaviour, BehaviourConfig};
use crate::physics::Layer;
use crate::z::ZIndex;
use crate::Despawn;

#[derive(Clone, Hash, Eq, PartialEq, Debug)]
pub(crate) enum Level {
    Level1,
}

#[derive(Component)]
pub(crate) struct InLevel;

#[derive(Default)]
pub(crate) struct LevelsPlugin;

impl Plugin for LevelsPlugin {
    fn build(&self, app: &mut App) {
        app.add_loading_state(
            LoadingState::new(MainGameState::Loading(Level::Level1))
                .continue_to_state(MainGameState::InGame(Level::Level1))
                .load_collection::<Background>()
                .load_collection::<Tiles>()
                .load_collection::<CharacterTexture>(),
        )
        .add_systems(OnEnter(MainGameState::InGame(Level::Level1)), spawn_level_1)
        .add_systems(OnExit(InGame), despawn_level);
    }
}

#[derive(AssetCollection, Resource)]
struct Background {
    #[asset(path = "oak/background/background_layer_1.png")]
    layer1: Handle<Image>,
    #[asset(path = "oak/background/background_layer_2.png")]
    layer2: Handle<Image>,
    #[asset(path = "oak/background/background_layer_3.png")]
    layer3: Handle<Image>,
}

impl Background {
    fn iter_with_z(&self) -> impl Iterator<Item = (Handle<Image>, f32)> {
        [
            self.layer1.clone(),
            self.layer2.clone(),
            self.layer3.clone(),
        ]
        .into_iter()
        .enumerate()
        .map(|(i, handle)| (handle, ZIndex::Background(i as u8 + 1).z()))
    }
}

#[derive(AssetCollection, Resource)]
pub(crate) struct Tiles {
    #[asset(path = "oak/floor.png")]
    floor: Handle<Image>,
    #[asset(path = "oak/platform.png")]
    platform: Handle<Image>,
    #[asset(path = "oak/box.png")]
    _box: Handle<Image>,
}

const FLOOR_Y: f32 = -80.0;
const FLOOR_LENGTH: f32 = 72.0;
const FLOOR_HEIGHT: f32 = 24.0;
const FLOORS_AMOUNT: i32 = 10;

const PLATFORM_LENGTH: f32 = 72.0;
const PLATFORM_HEIGHT: f32 = 12.0;

const PLATFORMS: [(f32, f32); 3] = [(0.0, -40.0), (60.0, 7.0), (-70.0, 40.0)];

const BG_LENGTH: f32 = 320.0;
const BGS_AMOUNT: i32 = 2;

fn despawn_level(items: Query<Entity, With<InLevel>>, mut despawner: EventWriter<Despawn>) {
    for item in items.iter() {
        despawner.send(Despawn(item));
    }
}

fn spawn_level_1(
    mut commands: Commands,
    tiles: Res<Tiles>,
    background: Res<Background>,
    mut character_spawner: EventWriter<SpawnCharacter>,
) {
    for x in (-FLOOR_LENGTH as i32 * FLOORS_AMOUNT..=FLOOR_LENGTH as i32 * FLOORS_AMOUNT)
        .step_by(FLOOR_LENGTH as usize)
    {
        commands.spawn((
            Sprite {
                image: tiles.floor.clone(),
                ..default()
            },
            Transform::from_translation(Vec3::new(x as f32, FLOOR_Y, ZIndex::Level.z())),
            Collider::rectangle(FLOOR_LENGTH as f32, FLOOR_HEIGHT as f32),
            RigidBody::Static,
            Layer::level(),
            InLevel,
        ));
    }

    for (x, y) in PLATFORMS {
        commands.spawn((
            Sprite {
                image: tiles.platform.clone(),
                ..default()
            },
            Transform::from_translation(Vec3::new(x, y, ZIndex::Level.z())),
            Collider::rectangle(PLATFORM_LENGTH as f32, PLATFORM_HEIGHT as f32),
            RigidBody::Static,
            Layer::level(),
            InLevel,
        ));
    }

    for x in
        (-BG_LENGTH as i32 * BGS_AMOUNT..=BG_LENGTH as i32 * BGS_AMOUNT).step_by(BG_LENGTH as usize)
    {
        for (texture, z) in background.iter_with_z() {
            commands.spawn((
                Sprite {
                    image: texture.clone(),
                    ..default()
                },
                Transform::from_translation(Vec3::new(x as f32, 0.0, z)),
                InLevel,
            ));
        }
    }

    character_spawner.send(SpawnCharacter {
        location: Vec2::ZERO,
        controlled: true,
        character_type: CharacterType::Combined,
        ..default()
    });

    character_spawner.send(SpawnCharacter {
        location: Vec2 { x: -100.0, y: 0.0 },
        controlled: false,
        character_type: CharacterType::Npc,
        behaviour: BehaviourConfig(vec![Behaviour::IdleForever]),
        debugger: true,
        ..default()
    });
}

const BOX_LENGTH: f32 = 12.0;

pub(crate) fn spawn_box(mut commands: Commands, tiles: Res<Tiles>, location: Vec2) {
    commands.spawn((
        Sprite {
            image: tiles._box.clone(),
            ..default()
        },
        Transform::from_translation(location.extend(ZIndex::Level.z())),
        Collider::rectangle(BOX_LENGTH as f32, BOX_LENGTH as f32),
        RigidBody::Dynamic,
        CollisionLayers::new([Layer::Box], [Layer::Level, Layer::Character, Layer::Box]),
        InLevel,
    ));
}
