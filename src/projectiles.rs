use crate::{
    character::{
        movement::KeyboardControlled,
        switching::{PlayerState, ShouldPause},
        visuals::CharacterTexture,
    },
    game_state::InGame,
    health::Health,
    levels::InLevel,
    physics::Layer,
    z::ZIndex,
    Despawn, SpriteChild,
};

use avian2d::prelude::*;
use bevy::{prelude::*, window::PrimaryWindow};

pub enum ProjectileDirection {
    Cursor,
    Target(Vec2),
}

#[derive(Event)]
pub struct SpawnProjectile {
    pub location: Vec2,
    pub flip_x: bool,
    pub speed: f32,
    pub damage: f32,
    pub shooter: Entity,
    pub direction: ProjectileDirection,
}

#[derive(Component)]
pub struct Projectile {
    pub damage: f32,
    pub shooter: Entity,
}

fn spawn_projectile(
    mut events: EventReader<SpawnProjectile>,
    mut commands: Commands,
    texture: Res<CharacterTexture>,
    windows: Query<&Window, With<PrimaryWindow>>,
    camera: Query<(&Camera, &GlobalTransform)>,
) {
    let Ok(window) = windows.get_single() else {
        return;
    };

    let Some(cursor) = window.cursor_position() else {
        return;
    };

    let Ok((camera, camera_transform)) = camera.get_single() else {
        return;
    };

    let Ok(ray) = camera.viewport_to_world(camera_transform, cursor) else {
        return;
    };

    let cursor_world_space = ray.origin.truncate();

    for ev in events.read() {
        let z = ZIndex::ControlledCharacter.z();

        let projectile_direction = match ev.direction {
            ProjectileDirection::Cursor => (cursor_world_space - ev.location).normalize(),
            ProjectileDirection::Target(target) => (target - ev.location).normalize(),
        };

        let child = commands
            .spawn((
                Sprite {
                    flip_x: ev.flip_x,
                    image: texture.projectile.clone(),
                    ..default()
                },
                Transform::from_translation(Vec2::ZERO.extend(z)),
            ))
            .id();

        let mut parent = commands.spawn((
            Transform::from_translation(ev.location.extend(z)),
            ShouldPause,
            SpriteChild(child),
        ));

        parent.insert((
            Layer::projectile(),
            Collider::rectangle(8.0, 3.0),
            LockedAxes::ROTATION_LOCKED,
            RigidBody::Dynamic,
            LinearVelocity::from(projectile_direction * ev.speed),
            Friction::new(2.5).with_static_coefficient(0.02),
            Projectile {
                damage: ev.damage,
                shooter: ev.shooter,
            },
            InLevel,
        ));

        parent.add_child(child);
    }
}

fn projectile_collision(
    spatial_query: SpatialQuery,
    projectiles: Query<(&Transform, &Collider, &Projectile, Entity)>,
    mut health_entities: Query<(&mut Health, Option<&KeyboardControlled>)>,
    mut despawner: EventWriter<Despawn>,
    mut exit: EventWriter<AppExit>,
) {
    for (transform, collider, projectile, projectile_entity) in projectiles.iter() {
        let intersections = spatial_query.shape_intersections(
            collider,
            transform.translation.truncate(),
            0.0,
            &SpatialQueryFilter::from_mask([Layer::Character, Layer::Level]),
        );

        let mut should_despawn = false;
        for entity in intersections {
            if entity == projectile.shooter {
                continue;
            }
            should_despawn = true;

            if let Ok((mut health, controlled)) = health_entities.get_mut(entity) {
                health.current -= projectile.damage;
                if health.current <= 0.0 {
                    despawner.send(Despawn(entity));
                    if controlled.is_some() {
                        exit.send(AppExit::Success);
                    }
                }
            }
        }

        if should_despawn {
            despawner.send(Despawn(projectile_entity));
        }
    }
}

#[derive(Default)]
pub struct ProjectilePlugin;

impl Plugin for ProjectilePlugin {
    fn build(&self, app: &mut App) {
        app.add_event::<SpawnProjectile>().add_systems(
            Update,
            (
                spawn_projectile.run_if(resource_exists::<CharacterTexture>),
                projectile_collision.run_if(not(in_state(PlayerState::Ghost))),
            )
                .run_if(in_state(InGame)),
        );
    }
}
