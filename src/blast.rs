use std::time::Duration;

use avian2d::prelude::*;
use bevy::{prelude::*, render::mesh::Mesh2d, sprite::MeshMaterial2d, time::Stopwatch};

use crate::{game_state::InGame, physics::Layer};

#[derive(Event)]
pub struct SpawnBlast {
    pub location: Vec2,
}

#[derive(Component)]
pub struct BlastIndicator {
    timer: Stopwatch,
}

fn spawn_blast(
    mut events: EventReader<SpawnBlast>,
    mut commands: Commands,
    spatial_query: SpatialQuery,
    transforms: Query<&Transform>,
    mut blast_indicator_spawner: EventWriter<SpawnBlastIndicator>,
    velocities: Query<&LinearVelocity>,
) {
    let radius: f32 = 50.0;
    let blast_force: f32 = 3000.0;

    for ev in events.read() {
        let intersections = spatial_query.shape_intersections(
            &Collider::circle(radius),
            ev.location,
            0.0,
            &SpatialQueryFilter::from_mask([Layer::Projectile, Layer::Box, Layer::Character]),
        );

        for entity in intersections.into_iter() {
            let Ok(target_loc) = transforms.get(entity) else {
                continue;
            };

            let target_loc = target_loc.translation.truncate();

            let loc_diff = target_loc - ev.location;
            let impulse = loc_diff.normalize_or_zero() * blast_force / loc_diff.length().max(16.0);

            let new_velocity = LinearVelocity(match velocities.get(entity) {
                Ok(velocity) => {
                    // sorry
                    let new_x = if impulse.x * velocity.0.x < 0.0 {
                        impulse.x
                    } else {
                        impulse.x + velocity.0.x
                    };

                    let new_y = if impulse.y * velocity.0.y < 0.0 {
                        impulse.y
                    } else {
                        impulse.y + velocity.0.y
                    };

                    Vec2::new(new_x, new_y)
                }
                _ => impulse,
            });

            commands.entity(entity).insert(new_velocity);
        }

        blast_indicator_spawner.send(SpawnBlastIndicator {
            location: ev.location,
        });
    }
}

fn spawn_blast_indicator(
    mut commands: Commands,
    mut evs: EventReader<SpawnBlastIndicator>,
    mut indicators: Query<&mut BlastIndicator>,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<ColorMaterial>>,
) {
    if evs.is_empty() {
        return;
    }
    let ev = evs.read().next().unwrap();

    if let Ok(mut indicator) = indicators.get_single_mut() {
        indicator.timer.reset();
    } else {
        commands.spawn((
            Mesh2d(meshes.add(Circle { radius: 10.0 })),
            MeshMaterial2d(materials.add(Color::Srgba(Srgba::WHITE))),
            Transform::from_translation(ev.location.extend(100.0)),
            BlastIndicator {
                timer: Stopwatch::new(),
            },
        ));
    }

    evs.clear();
}

fn animate_blast_indicator(
    mut indicators: Query<(Entity, &MeshMaterial2d<ColorMaterial>, &mut BlastIndicator)>,
    mut materials: ResMut<Assets<ColorMaterial>>,
    time: Res<Time>,
    mut commands: Commands,
) {
    let blast_indicator_time: Duration = Duration::from_secs_f32(0.3);

    let Ok((entity, material_handle, mut indicator)) = indicators.get_single_mut() else {
        return;
    };

    indicator.timer.tick(time.delta());

    if indicator.timer.elapsed() >= blast_indicator_time {
        commands.entity(entity).despawn();
    } else {
        let alpha = 1.0
            - indicator
                .timer
                .elapsed()
                .div_duration_f32(blast_indicator_time);

        let material = materials.get_mut(material_handle).unwrap();
        material.color = Color::Srgba(Srgba::new(1.0, 1.0, 1.0, alpha));
    }
}

#[derive(Default)]
pub struct BlastPlugin;

#[derive(Event)]
pub struct SpawnBlastIndicator {
    pub location: Vec2,
}

impl Plugin for BlastPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(
            Update,
            (spawn_blast, spawn_blast_indicator, animate_blast_indicator).run_if(in_state(InGame)),
        )
        .add_event::<SpawnBlast>()
        .add_event::<SpawnBlastIndicator>();
    }
}
