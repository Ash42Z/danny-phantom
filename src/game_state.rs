use bevy::prelude::*;

use crate::levels::Level;

#[derive(States, Clone, Hash, Eq, PartialEq, Debug, Default)]
pub(crate) enum MainGameState {
    #[default]
    MainMenu,
    Loading(Level),
    InGame(Level),
}

#[derive(Clone, PartialEq, Eq, Hash, Debug)]
pub(crate) struct InGame;

impl ComputedStates for InGame {
    type SourceStates = MainGameState;

    fn compute(main: MainGameState) -> Option<Self> {
        matches!(main, MainGameState::InGame(_)).then_some(Self)
    }
}

#[derive(Default)]
pub(crate) struct GameStatePlugin;

impl Plugin for GameStatePlugin {
    fn build(&self, app: &mut App) {
        app.init_state::<MainGameState>()
            .add_computed_state::<InGame>();
    }
}
