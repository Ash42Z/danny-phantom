mod blast;
mod character;
mod debug;
mod dialogue;
mod game_state;
mod health;
mod inventory;
mod levels;
mod menu;
mod npc;
mod physics;
mod projectiles;
mod z;

use crate::character::switching::RecordBlast;
use crate::physics::PhysicsPlugin;

use bevy::prelude::*;

use blast::BlastPlugin;
use character::CharacterPlugin;
use debug::DebugPlugin;
use dialogue::DialoguePlugin;
use game_state::GameStatePlugin;
use health::HealthPlugin;
use inventory::InventoryPlugin;
use levels::LevelsPlugin;
use menu::MenuPlugin;
use npc::NpcPlugin;
use projectiles::ProjectilePlugin;

#[derive(Component)]
struct SpriteChild(Entity);

#[derive(Event)]
struct Despawn(Entity);

fn despawner(mut commands: Commands, mut evs: EventReader<Despawn>) {
    for ev in evs.read() {
        commands.entity(ev.0).despawn_recursive();
    }
}

const DEFAULT_SCALE: f32 = 180.0;

fn spawn_camera(mut commands: Commands) {
    commands.spawn((
        Camera2d::default(),
        OrthographicProjection {
            scaling_mode: bevy::render::camera::ScalingMode::FixedVertical {
                viewport_height: DEFAULT_SCALE,
            },
            ..OrthographicProjection::default_2d()
        },
        Transform::from_translation(Vec3::new(0.0, 0.0, 100 as f32)),
    ));
}

fn main() {
    App::new()
        .add_plugins(DefaultPlugins.set(ImagePlugin::default_nearest()))
        .add_systems(Startup, spawn_camera)
        .add_plugins(DebugPlugin::default())
        .add_plugins(PhysicsPlugin::default())
        .add_plugins(GameStatePlugin::default())
        .add_plugins(MenuPlugin::default())
        .add_plugins(LevelsPlugin::default())
        .add_plugins(CharacterPlugin::default())
        .add_plugins(ProjectilePlugin::default())
        .add_plugins(BlastPlugin::default())
        .add_plugins(NpcPlugin::default())
        .add_plugins(HealthPlugin::default())
        .add_plugins(DialoguePlugin::default())
        .add_plugins(InventoryPlugin::default())
        .add_event::<RecordBlast>()
        .add_event::<Despawn>()
        .add_systems(PostUpdate, despawner)
        .run();
}
