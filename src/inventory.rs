use bevy::color::palettes::tailwind::*;
use bevy::color::ColorRange;
use bevy::prelude::*;

use crate::character::action::ReloadTimer;
use crate::{game_state::InGame, Despawn};
#[derive(Resource, Default)]
pub struct InventoryPlugin;

impl Plugin for InventoryPlugin {
    fn build(&self, app: &mut App) {
        app.insert_resource(Inventory::default())
            .add_systems(OnEnter(InGame), spawn_inventory_ui)
            .add_systems(OnExit(InGame), despawn_inventory_ui)
            .add_systems(Update, update_inventory_ui.run_if(in_state(InGame)));
    }
}

const DEFAULT_AMMUNITION: u64 = 5;

#[derive(Resource)]
pub struct Inventory {
    pub max_ammunition: u64,
    pub ammunition: u64,
}

impl Default for Inventory {
    fn default() -> Self {
        Self {
            max_ammunition: DEFAULT_AMMUNITION,
            ammunition: DEFAULT_AMMUNITION,
        }
    }
}

impl Inventory {
    pub fn reload(&mut self) {
        self.ammunition = self.max_ammunition;
    }
}

#[derive(Component)]
pub struct InventoryUI;

pub fn spawn_inventory_ui(mut commands: Commands) {
    commands
        .spawn(Node {
            flex_direction: FlexDirection::Column,
            justify_content: JustifyContent::End,
            align_items: AlignItems::Start,
            height: Val::Vh(100.0),
            padding: UiRect::all(Val::Px(24.0)),
            ..default()
        })
        .with_child((Text::default(), TextFont::from_font_size(64.0), InventoryUI));
}

pub fn despawn_inventory_ui(
    ui: Query<Entity, With<InventoryUI>>,
    mut despawner: EventWriter<Despawn>,
) {
    if let Ok(entity) = ui.get_single() {
        despawner.send(Despawn(entity));
    }
}

pub fn update_inventory_ui(
    inventory: Res<Inventory>,
    reload_timer: Query<&ReloadTimer>,
    mut text: Query<(&mut Text, &mut TextColor), With<InventoryUI>>,
) {
    let Ok((mut text, mut color)) = text.get_single_mut() else {
        return;
    };

    if let Ok(ReloadTimer(timer)) = reload_timer.get_single() {
        let progress = timer.elapsed_secs() / timer.duration().as_secs_f32();
        let color_range = LIME_500..LIME_200;

        let reloaded_ammo: u64 = inventory.ammunition
            + ((inventory.max_ammunition - inventory.ammunition) as f32 * progress).floor() as u64;
        *text = Text::new(reloaded_ammo.to_string());
        color.0 = color_range.at(progress).into();
    } else {
        *text = Text::new(inventory.ammunition.to_string());
        color.0 = Color::WHITE;
    }
}
