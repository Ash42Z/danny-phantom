use std::time::Duration;

use bevy::{
    color::palettes::{css::WHITE, tailwind::ORANGE_500},
    prelude::*,
    render::mesh::Mesh2d,
    sprite::MeshMaterial2d,
};
use bevy_yarnspinner::{
    events::{DialogueCompleteEvent, DialogueStartEvent},
    prelude::*,
};
use bevy_yarnspinner_example_dialogue_view::prelude::*;

use crate::{
    character::{movement::KeyboardControlled, CharacterType, SpawnCharacter},
    game_state::InGame,
    npc::{Behaviour, BehaviourConfig},
};

#[derive(Default)]
pub struct DialoguePlugin;

#[derive(States, Clone, Eq, PartialEq, Hash, Debug, Default)]
enum Dialogue {
    #[default]
    Closed,
    Open,
}

#[derive(Component)]
pub(crate) struct DialogueFile(pub(crate) &'static str);

fn change_state_to_open(mut next_state: ResMut<NextState<Dialogue>>) {
    next_state.set(Dialogue::Open);
}

fn change_state_to_closed(mut next_state: ResMut<NextState<Dialogue>>) {
    next_state.set(Dialogue::Closed);
}

fn setup_runner(mut commands: Commands, project: Res<YarnProject>) {
    let mut dialogue_runner = project.create_dialogue_runner();
    dialogue_runner
        .commands_mut()
        .add_command("spawn_enemy", spawn_enemy);
    commands.spawn(dialogue_runner);
}

const MINIMUM_DISTANCE: i32 = 35;

#[derive(Component)]
struct DialogueTarget;

fn sync_target(
    character: Query<&Transform, With<KeyboardControlled>>,
    current: Query<Entity, With<DialogueTarget>>,
    talkable: Query<(Entity, &Transform), With<DialogueFile>>,
    mut commands: Commands,
) {
    let Ok(character) = character.get_single() else {
        return;
    };

    let character = character.translation.truncate();

    let to_talk = talkable
        .iter()
        .map(|(df, transform)| {
            (
                df,
                transform.translation.truncate().distance(character) as i32,
            )
        })
        .filter(|(_, distance)| *distance <= MINIMUM_DISTANCE)
        .min_by_key(|(_, distance)| *distance)
        .map(|(entity, _)| entity);

    let current = current.get_single().ok();

    match (to_talk, current) {
        (Some(new_target), None) => {
            commands.entity(new_target).insert(DialogueTarget);
        }
        (Some(new_target), Some(current)) if new_target != current => {
            commands.entity(current).remove::<DialogueTarget>();
            commands.entity(new_target).insert(DialogueTarget);
        }
        (None, Some(current)) => {
            commands.entity(current).remove::<DialogueTarget>();
        }
        _ => (),
    }
}

fn color_new_target(
    target: Query<Entity, Added<DialogueTarget>>,
    indicator: Query<(&Parent, Entity), With<DialogueIndicator>>,
    mut materials: ResMut<Assets<ColorMaterial>>,
    mut commands: Commands,
) {
    let Ok(target) = target.get_single() else {
        return;
    };

    for (parent, entity) in indicator.iter() {
        if parent.get() == target {
            commands
                .entity(entity)
                .insert(MeshMaterial2d(materials.add(Color::Srgba(ORANGE_500))));
        }
    }
}

fn remove_target_color(
    mut target: RemovedComponents<DialogueTarget>,
    indicator: Query<(&Parent, Entity), With<DialogueIndicator>>,
    mut materials: ResMut<Assets<ColorMaterial>>,
    mut commands: Commands,
) {
    for target in target.read() {
        for (parent, entity) in indicator.iter() {
            if parent.get() == target {
                commands
                    .entity(entity)
                    .insert(MeshMaterial2d(materials.add(Color::Srgba(WHITE))));
            }
        }
    }
}

fn open_dialogue(
    target: Query<&DialogueFile, With<DialogueTarget>>,
    mut runner: Query<&mut DialogueRunner>,
    keys: Res<ButtonInput<KeyCode>>,
) {
    if keys.just_pressed(KeyCode::KeyC) {
        let Ok(target) = target.get_single() else {
            return;
        };

        let Ok(mut runner) = runner.get_single_mut() else {
            return;
        };

        runner.start_node(target.0);
    }
}

fn spawn_enemy(In(()): In<()>, mut character_spawner: EventWriter<SpawnCharacter>) {
    let mut timer = Timer::new(Duration::from_secs(0), TimerMode::Once);
    timer.tick(Duration::from_secs(1));

    character_spawner.send(SpawnCharacter {
        location: Vec2 { x: 100.0, y: 0.0 },
        controlled: false,
        character_type: CharacterType::Enemy,
        behaviour: BehaviourConfig(vec![
            Behaviour::FollowPlayer {
                speed: 92.0,
                target_distance: 60.0,
                activation_distance: 200.0,
            },
            Behaviour::ShootInRange {
                projectile_speed: 180.0,
                range: 100.0,
                projectile_damage: 10.0,
                attack_speed: Duration::from_secs(1),
                timer,
            },
        ]),
        ..default()
    });
}

#[derive(Component)]
struct DialogueIndicator;

fn spawn_dialogue_indicator(
    new_talkable: Query<Entity, Added<DialogueFile>>,
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<ColorMaterial>>,
) {
    for entity in new_talkable.iter() {
        let shape = Mesh2d(meshes.add(Triangle2d::new(
            Vec2::new(0.0, 5.0),
            Vec2::new(6.0, 10.0),
            Vec2::new(-6.0, 10.0),
        )));
        let color = Color::Srgba(WHITE);

        commands.entity(entity).with_children(|parent| {
            parent.spawn((
                DialogueIndicator,
                shape,
                MeshMaterial2d(materials.add(color)),
                Transform::from_xyz(0.0, 20.0, 5.0),
            ));
        });
    }
}

impl Plugin for DialoguePlugin {
    fn build(&self, app: &mut App) {
        app.add_plugins((
            YarnSpinnerPlugin::new(),
            ExampleYarnSpinnerDialogueViewPlugin::new(),
        ))
        .init_state::<Dialogue>()
        .add_systems(
            Update,
            (
                open_dialogue.run_if(in_state(Dialogue::Closed)),
                change_state_to_open.run_if(on_event::<DialogueStartEvent>),
                spawn_dialogue_indicator,
                sync_target,
                color_new_target,
                remove_target_color,
            )
                .run_if(in_state(InGame)),
        )
        .add_systems(
            Update,
            (
                change_state_to_closed.run_if(on_event::<DialogueCompleteEvent>),
                setup_runner.run_if(resource_added::<YarnProject>),
            ),
        );
    }
}
