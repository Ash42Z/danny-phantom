use rand::Rng;

pub(crate) enum ZIndex {
    Background(u8),
    Level,
    Character(f32),
    ControlledCharacter,
    Ui,
}

impl ZIndex {
    pub(crate) fn character() -> Self {
        Self::Character(Self::rand_offset())
    }

    pub(crate) fn z(&self) -> f32 {
        match self {
            Self::Background(stack) => *stack as f32 / 256.0,
            Self::Level => 1.0,
            Self::Character(offset) => 2.0 + offset,
            Self::ControlledCharacter => 3.0,
            Self::Ui => 4.0,
        }
    }

    pub(crate) fn rand_offset() -> f32 {
        let mut rng = rand::thread_rng();
        rng.gen_range(0.0..1.0)
    }
}
