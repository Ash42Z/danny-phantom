pub mod visuals;
use std::time::Duration;

use bevy_tnua::control_helpers::TnuaSimpleAirActionsCounter;
use movement::KeyboardControlled;
use std::collections::HashMap;
use visuals::{
    animate_character, dash_opacity, switch_character_animation, CharacterAnimation,
    CharacterAnimationId, CharacterAnimationRegistry, CharacterAnimationTracker, CharacterTexture,
};

pub mod movement;
use movement::{camera_follow, movement};

pub mod switching;
use switching::{
    despawn_record_timer_ui, ghost_timer, handle_record, handle_replay, pause,
    spawn_record_timer_ui, switch_to_combined, switch_to_ghost, switch_to_human,
    toggle_player_character, unpause, update_record_timer_ui, Ghost, Human, Paused, PlayerState,
    ShouldPause,
};

pub mod action;
use action::{handle_reload_timer, player_action, Reloading};

use avian2d::prelude::*;
use bevy::color::palettes::tailwind::*;
use bevy::prelude::*;
use bevy::render::mesh::Mesh2d;
use bevy::sprite::MeshMaterial2d;
use bevy_tnua::prelude::*;
use bevy_tnua_avian2d::TnuaAvian2dPlugin;

use crate::dialogue::DialogueFile;
use crate::game_state::InGame;
use crate::health::{Health, HealthBar};
use crate::levels::InLevel;
use crate::npc::{BehaviourConfig, Npc};
use crate::physics::Layer;
use crate::z::ZIndex;
use crate::SpriteChild;

pub const CHARACTER_COLLIDER: Vec2 = Vec2 { x: 9.0, y: 11.0 };
pub const CHARACTER_SPRITE_OFFSET: Vec2 = Vec2 { x: 0.0, y: 10.0 };

#[derive(Default)]
pub struct CharacterPlugin;

#[derive(Event)]
pub struct SpawnCharacter {
    pub location: Vec2,
    pub controlled: bool,
    pub character_type: CharacterType,
    pub flip_x: bool,
    pub physics: bool,
    pub behaviour: BehaviourConfig,
    pub debugger: bool,
    pub health: f32,
}

#[derive(PartialEq, Eq)]
pub enum CharacterType {
    Combined,
    Human,
    Ghost,
    Enemy,
    Npc,
}

impl CharacterType {
    pub fn color(&self) -> Color {
        Color::Srgba(match self {
            Self::Combined => NEUTRAL_50,
            Self::Human => BLUE_500,
            Self::Ghost => GREEN_500,
            Self::Enemy => RED_500,
            Self::Npc => YELLOW_500,
        })
    }
}

impl Default for SpawnCharacter {
    fn default() -> Self {
        Self {
            location: Vec2::ZERO,
            controlled: false,
            character_type: CharacterType::Combined,
            flip_x: false,
            physics: true,
            behaviour: BehaviourConfig::default(),
            debugger: false,
            health: 100_f32,
        }
    }
}

fn spawn_characters(
    mut events: EventReader<SpawnCharacter>,
    mut commands: Commands,
    texture: Res<CharacterTexture>,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<ColorMaterial>>,
    paused: Option<Res<State<Paused>>>,
    animations: Res<CharacterAnimationRegistry>,
) {
    for ev in events.read() {
        let z = if ev.controlled {
            ZIndex::ControlledCharacter
        } else {
            ZIndex::character()
        }
        .z();

        let character_sprite_entity = commands
            .spawn((
                Sprite {
                    color: ev.character_type.color(),
                    flip_x: ev.flip_x,
                    texture_atlas: Some(TextureAtlas {
                        layout: texture.atlas.clone(),
                        index: 1,
                    }),
                    image: texture.texture.clone(),
                    ..default()
                },
                Transform::from_translation(CHARACTER_SPRITE_OFFSET.extend(z)),
                CharacterAnimationTracker::new(
                    CharacterAnimationId::Idle,
                    &animations,
                    paused.is_some() && ev.character_type != CharacterType::Ghost,
                ),
            ))
            .id();

        let blast_circle_entity = commands
            .spawn((
                Mesh2d(meshes.add(Circle { radius: 50.0 })),
                MeshMaterial2d(materials.add(Color::Srgba(CYAN_500.with_alpha(0.1)))),
            ))
            .id();

        let health_bar_entity = HealthBar::spawn(100_f32, &mut commands).id();

        let mut parent = commands.spawn((
            Transform::from_translation(ev.location.extend(z)),
            ShouldPause,
            SpriteChild(character_sprite_entity),
            Health {
                current: ev.health,
                max: 100.0,
                bar: health_bar_entity,
            },
            InLevel,
        ));

        if ev.physics {
            parent.insert((
                TnuaController::default(),
                ev.behaviour.clone(),
                Layer::character(),
                Collider::capsule(CHARACTER_COLLIDER.x, CHARACTER_COLLIDER.y),
                LockedAxes::ROTATION_LOCKED,
                if paused.is_some() && ev.character_type != CharacterType::Ghost {
                    RigidBody::Static
                } else {
                    RigidBody::Dynamic
                },
                TnuaSimpleAirActionsCounter::default(),
            ));
        }

        if ev.controlled {
            parent.insert(KeyboardControlled);
        }

        if ev.debugger {
            parent.insert(DialogueFile("Debugger"));
        }

        match ev.character_type {
            CharacterType::Human => {
                parent.insert(Human);
            }
            CharacterType::Ghost => {
                parent.insert(Ghost);
            }
            CharacterType::Enemy | CharacterType::Npc => {
                parent.insert(Npc);
            }
            _ => {}
        }

        parent.add_child(character_sprite_entity);
        parent.add_child(blast_circle_entity);
        parent.add_child(health_bar_entity);
    }
}

fn setup(mut commands: Commands) {
    let registry: HashMap<CharacterAnimationId, CharacterAnimation> = [
        (
            CharacterAnimationId::Idle,
            CharacterAnimation {
                duration: Duration::from_secs_f32(0.1),
                sprite_range: 0..=5,
                repeated: true,
            },
        ),
        (
            CharacterAnimationId::Walk,
            CharacterAnimation {
                duration: Duration::from_secs_f32(0.1),
                sprite_range: 16..=23,
                repeated: true,
            },
        ),
        (
            CharacterAnimationId::Jump,
            CharacterAnimation {
                duration: Duration::from_secs_f32(0.1),
                sprite_range: 24..=31,
                repeated: false,
            },
        ),
        (
            CharacterAnimationId::Fall,
            CharacterAnimation {
                duration: Duration::from_secs_f32(0.1),
                sprite_range: 32..=36,
                repeated: false,
            },
        ),
        (
            CharacterAnimationId::Land,
            CharacterAnimation {
                duration: Duration::from_secs_f32(0.1),
                sprite_range: 37..=39,
                repeated: false,
            },
        ),
    ]
    .into_iter()
    .collect();

    commands.insert_resource(CharacterAnimationRegistry(registry));
}

impl Plugin for CharacterPlugin {
    fn build(&self, app: &mut App) {
        app.add_plugins((
            TnuaControllerPlugin::new(FixedUpdate),
            TnuaAvian2dPlugin::new(FixedUpdate),
        ))
        .insert_state(PlayerState::default())
        .add_sub_state::<Reloading>()
        .add_computed_state::<Paused>()
        .add_event::<SpawnCharacter>()
        .add_systems(Startup, setup)
        .add_systems(OnEnter(Paused), pause)
        .add_systems(OnExit(Paused), unpause)
        .add_systems(OnEnter(PlayerState::Combined), switch_to_combined)
        .add_systems(
            OnEnter(PlayerState::Ghost),
            (switch_to_ghost, spawn_record_timer_ui),
        )
        .add_systems(OnEnter(PlayerState::Human), switch_to_human)
        .add_systems(OnExit(PlayerState::Ghost), despawn_record_timer_ui)
        .add_systems(
            Update,
            (
                handle_replay.run_if(in_state(PlayerState::Human)),
                handle_record.run_if(in_state(PlayerState::Ghost)),
                (ghost_timer, update_record_timer_ui).run_if(in_state(PlayerState::Ghost)),
                spawn_characters.run_if(resource_exists::<CharacterTexture>),
                switch_character_animation,
                animate_character,
                dash_opacity,
                toggle_player_character.run_if(not(in_state(Reloading::Yes))),
                movement,
                player_action.run_if(not(in_state(Reloading::Yes))),
                handle_reload_timer.run_if(in_state(Reloading::Yes)),
                camera_follow,
            )
                .run_if(in_state(InGame)),
        );
    }
}
