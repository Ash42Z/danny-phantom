use std::{collections::HashMap, ops::RangeInclusive, time::Duration};

use bevy::prelude::*;
use bevy_asset_loader::asset_collection::AssetCollection;
use bevy_tnua::{builtins::TnuaBuiltinDash, controller::TnuaController, TnuaAction};

#[derive(AssetCollection, Resource)]
pub(crate) struct CharacterTexture {
    #[asset(texture_atlas_layout(tile_size_x = 56, tile_size_y = 56, columns = 8, rows = 7))]
    pub(crate) atlas: Handle<TextureAtlasLayout>,
    #[asset(path = "oak/character/char_blue.png")]
    pub(crate) texture: Handle<Image>,
    #[asset(path = "oak/decorations/grass_1.png")]
    pub(crate) projectile: Handle<Image>,
}

#[derive(Copy, Clone, PartialEq, Eq, Debug, Hash, Default)]
pub enum CharacterAnimationId {
    #[default]
    Idle,
    Walk,
    Jump,
    Fall,
    Land,
}

#[derive(Component, Debug)]
pub struct CharacterAnimationTracker {
    pub animation: CharacterAnimationId,
    pub timer: Timer,
    pub paused: bool,
}

impl CharacterAnimationTracker {
    pub fn new(
        animation: CharacterAnimationId,
        animations: &CharacterAnimationRegistry,
        paused: bool,
    ) -> Self {
        Self {
            animation,
            timer: animations.0[&animation].new_timer(),
            paused,
        }
    }
}

pub struct CharacterAnimation {
    pub duration: Duration,
    pub sprite_range: RangeInclusive<usize>,
    pub repeated: bool,
}

impl CharacterAnimation {
    fn new_timer(&self) -> Timer {
        let mode = if self.repeated {
            TimerMode::Repeating
        } else {
            TimerMode::Once
        };

        Timer::new(self.duration, mode)
    }
}

#[derive(Resource)]
pub struct CharacterAnimationRegistry(pub HashMap<CharacterAnimationId, CharacterAnimation>);

pub fn animate_character(
    time: Res<Time>,
    mut sprites: Query<(&mut CharacterAnimationTracker, &mut Sprite)>,
    animations: Res<CharacterAnimationRegistry>,
) {
    for (mut sprite_animation, mut sprite) in sprites.iter_mut() {
        let Some(atlas) = &mut sprite.texture_atlas else {
            continue;
        };

        if sprite_animation.paused {
            continue;
        }

        if sprite_animation.timer.tick(time.delta()).just_finished() {
            let Some(info) = animations.0.get(&sprite_animation.animation) else {
                continue;
            };

            atlas.index = if info.sprite_range.contains(&(atlas.index + 1)) {
                sprite_animation.timer.reset();
                atlas.index + 1
            } else {
                if info.repeated {
                    sprite_animation.timer.reset();
                    *info.sprite_range.start()
                } else {
                    *info.sprite_range.end()
                }
            };
        }
    }
}

pub fn switch_character_animation(
    mut characters: Query<(&mut CharacterAnimationTracker, &mut Sprite, &Parent)>,
    controllers: Query<&TnuaController>,
    animations: Res<CharacterAnimationRegistry>,
) {
    for (mut tracker, mut sprite, parent) in characters.iter_mut() {
        let parent = parent.get();
        let Ok(controller) = controllers.get(parent) else {
            continue;
        };

        let Some(atlas) = &mut sprite.texture_atlas else {
            continue;
        };

        let animation = controller
            .dynamic_basis()
            .map_or(CharacterAnimationId::Idle, |basis| {
                let Vec3 { x, y, .. } = basis.effective_velocity();

                if basis.is_airborne() {
                    if y < 0.0 {
                        CharacterAnimationId::Fall
                    } else {
                        CharacterAnimationId::Jump
                    }
                } else {
                    if x.abs() < 30.0 {
                        CharacterAnimationId::Idle
                    } else {
                        CharacterAnimationId::Walk
                    }
                }
            });

        if animation != tracker.animation {
            tracker.animation = animation;
            let info = &animations.0[&animation];
            tracker.timer = info.new_timer();
            atlas.index = *info.sprite_range.start();
        }
    }
}

pub fn dash_opacity(
    mut characters: Query<(&mut Sprite, &Parent)>,
    controllers: Query<&TnuaController>,
) {
    for (mut sprite, parent) in characters.iter_mut() {
        let parent = parent.get();
        if let Ok(controller) = controllers.get(parent) {
            if controller.action_name() == Some(TnuaBuiltinDash::NAME) {
                sprite.color.set_alpha(0.5);
            } else {
                sprite.color.set_alpha(1.0);
            }
        }
    }
}
