use bevy::prelude::*;
use bevy_tnua::{
    builtins::{TnuaBuiltinDash, TnuaBuiltinJump, TnuaBuiltinWalk},
    control_helpers::TnuaSimpleAirActionsCounter,
    controller::TnuaController,
    TnuaAction,
};

use crate::SpriteChild;

use super::switching::PlayerState;

const DASH_DISPLACEMENT: f32 = 50.0;
const DASH_SPEED: f32 = 300.0;

pub fn camera_follow(
    mut camera: Query<
        (&mut Transform, &OrthographicProjection),
        (With<Camera>, Without<KeyboardControlled>),
    >,
    character: Query<&Transform, (With<KeyboardControlled>, Without<Camera>)>,
    time: Res<Time>,
) {
    let Ok((mut camera, projection)) = camera.get_single_mut() else {
        return;
    };

    let Ok(character) = character.get_single() else {
        return;
    };

    let x_low = projection.area.min.x + camera.translation.x;
    let x_high = projection.area.max.x + camera.translation.x;
    let y_low = projection.area.min.y + camera.translation.y;
    let y_high = projection.area.max.y + camera.translation.y;

    let min_character_x = (x_high - x_low) * 0.3 + x_low;
    let max_character_x = (x_high - x_low) * 0.7 + x_low;
    let min_character_y = (y_high - y_low) * 0.4 + y_low;
    let max_character_y = (y_high - y_low) * 0.7 + y_low;

    // TODO: make this prettier
    let max_speed: f32 = 3000.0;
    if character.translation.x < min_character_x {
        let distance = min_character_x - character.translation.x;
        camera.translation.x -= 0.5 * (distance * distance).min(max_speed) * time.delta_secs();
    } else if character.translation.x > max_character_x {
        let distance = character.translation.x - max_character_x;
        camera.translation.x += 0.5 * (distance * distance).min(max_speed) * time.delta_secs();
    }

    if character.translation.y
        > (projection.area.max.y - projection.area.min.y) * 0.1 + projection.area.min.y
        && character.translation.y
            < (projection.area.max.y - projection.area.min.y) * 0.9 + projection.area.min.y
    {
        let distance = -camera.translation.y;
        camera.translation.y += 4.0 * distance * time.delta_secs();
    } else {
        if character.translation.y < min_character_y {
            let distance = min_character_y - character.translation.y;
            camera.translation.y -= 0.5 * (distance * distance).min(max_speed) * time.delta_secs();
        } else if character.translation.y > max_character_y {
            let distance = character.translation.y - max_character_y;
            camera.translation.y += 0.5 * (distance * distance).min(max_speed) * time.delta_secs();
        }
    }
}

#[derive(Component)]
pub struct KeyboardControlled;

pub fn movement(
    keys: Res<ButtonInput<KeyCode>>,
    mut controlled: Query<
        (&mut TnuaController, &mut TnuaSimpleAirActionsCounter, &SpriteChild),
        With<KeyboardControlled>,
    >,
    player_state: Res<State<PlayerState>>,
    mut sprites: Query<&mut Sprite>,
) {
    let Ok((mut controller, mut air_tracker, sprite_ptr)) = controlled.get_single_mut() else {
        return;
    };
    let Ok(mut sprite) = sprites.get_mut(sprite_ptr.0) else {
        return;
    };

    air_tracker.update(&controller);

    let mut movement_direction = Vec2::ZERO;

    if keys.pressed(KeyCode::KeyD) {
        movement_direction += Vec2::X;
        sprite.flip_x = false;
    }
    if keys.pressed(KeyCode::KeyA) {
        movement_direction -= Vec2::X;
        sprite.flip_x = true;
    }

    movement_direction = movement_direction.clamp_length_max(1.0);

    let jump = keys.just_pressed(KeyCode::Space);

    controller.basis(TnuaBuiltinWalk {
        desired_velocity: (movement_direction * player_state.character_move_speed()).extend(0.0),
        desired_forward: Dir3::new(movement_direction.normalize_or_zero().extend(0.0)).ok(),
        float_height: 18.2,
        acceleration: 1000.0,
        air_acceleration: 700.0,
        ..default()
    });

    let should_jump = jump
        && match player_state.get() {
            PlayerState::Combined | PlayerState::Ghost => {
                air_tracker.air_count_for(TnuaBuiltinJump::NAME) < 2
            }
            PlayerState::Human => air_tracker.air_count_for(TnuaBuiltinJump::NAME) < 1,
        };
    if should_jump {
        controller.action(TnuaBuiltinJump {
            height: 50.0,
            allow_in_air: true,
            ..default()
        });
    }

    let dash = keys.just_pressed(KeyCode::ShiftLeft);
    let dash_back = keys.just_pressed(KeyCode::KeyQ);
    if *player_state.get() == PlayerState::Combined
        && air_tracker.air_count_for(TnuaBuiltinDash::NAME) < 2
        && (dash || dash_back)
    {
        let dash_direction = if dash ^ sprite.flip_x { Vec2::X } else { -Vec2::X };

        controller.action(TnuaBuiltinDash {
            allow_in_air: true,
            displacement: (dash_direction * DASH_DISPLACEMENT).extend(0.0),
            speed: DASH_SPEED,
            brake_to_speed: player_state.character_move_speed(),
            acceleration: 4000.0,
            ..default()
        });
    }
}
