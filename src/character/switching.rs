use std::collections::VecDeque;
use std::time::Duration;

use avian2d::prelude::{LinearVelocity, RigidBody};
use bevy::{prelude::*, time::Stopwatch};

use crate::blast::{SpawnBlast, SpawnBlastIndicator};
use crate::health::Health;
use crate::{Despawn, SpriteChild};

use super::{
    movement::KeyboardControlled, visuals::CharacterAnimationTracker, CharacterType, SpawnCharacter,
};

#[derive(Clone, Eq, PartialEq, Debug, Hash, Default, States)]
pub enum PlayerState {
    #[default]
    Combined,
    Ghost,
    Human,
}

impl PlayerState {
    pub fn bullet_speed(&self) -> f32 {
        match self {
            Self::Combined => 480.0,
            Self::Human => 160.0,
            _ => 0.0,
        }
    }

    pub fn character_move_speed(&self) -> f32 {
        match self {
            Self::Combined => 100.0,
            Self::Human => 92.0,
            Self::Ghost => 150.0,
        }
    }
}

#[derive(Clone, Eq, PartialEq, Debug, Hash, Default)]
pub struct Paused;

impl ComputedStates for Paused {
    type SourceStates = PlayerState;

    fn compute(state: PlayerState) -> Option<Self> {
        match state {
            PlayerState::Ghost => Some(Self),
            _ => None,
        }
    }
}

#[derive(Component)]
pub struct ShouldPause;

#[derive(Component)]
pub struct ResumeState {
    velocity: Vec2,
}

#[derive(Clone, Debug)]
pub struct CharacterLocationAndAnimation {
    location: Vec2,
    atlas_index: usize,
    flip_x: bool,
}

#[derive(Clone, Debug)]
pub struct RecordedInstant {
    character: CharacterLocationAndAnimation,
    blasting: bool,
}

#[derive(Clone, Debug)]
pub struct RecordState {
    timer: Stopwatch,
    state_stack: VecDeque<(f32, RecordedInstant)>,
}

#[derive(Clone, Resource, Debug)]
pub struct Recording(RecordState);

#[derive(Clone, Resource, Debug)]
pub struct Replaying(RecordState);

#[derive(Event)]
pub struct RecordBlast;

#[derive(Component, Clone)]
pub struct Human;

#[derive(Component, Clone)]
pub struct Ghost;

pub fn handle_record(
    ghost: Query<(&Transform, &SpriteChild), With<Ghost>>,
    mut record_state: ResMut<Recording>,
    mut blast_recordings: EventReader<RecordBlast>,
    mut blast_indicator_spawner: EventWriter<SpawnBlastIndicator>,
    sprites: Query<&Sprite, Without<KeyboardControlled>>,
    time: Res<Time>,
) {
    let Ok((transform, sprite_ptr)) = ghost.get_single() else {
        return;
    };

    let Ok(sprite) = sprites.get(sprite_ptr.0) else {
        return;
    };

    let Some(atlas) = &sprite.texture_atlas else {
        return;
    };

    record_state.0.timer.tick(time.delta());
    let elapsed = record_state.0.timer.elapsed_secs();

    record_state.0.state_stack.push_back((
        elapsed,
        RecordedInstant {
            character: CharacterLocationAndAnimation {
                location: transform.translation.truncate(),
                atlas_index: atlas.index,
                flip_x: sprite.flip_x,
            },
            blasting: !blast_recordings.is_empty(),
        },
    ));

    if !blast_recordings.is_empty() {
        blast_indicator_spawner.send(SpawnBlastIndicator {
            location: transform.translation.truncate(),
        });
    }

    blast_recordings.clear();
}

trait Alter {
    type Inner;

    fn alter<F>(&mut self, f: F)
    where
        F: FnOnce(&mut Self::Inner);
}

impl<T> Alter for Option<T> {
    type Inner = T;

    fn alter<F>(&mut self, f: F)
    where
        F: FnOnce(&mut Self::Inner),
    {
        if let Some(me) = self {
            f(me)
        }
    }
}

pub fn handle_replay(
    mut ghost: Query<(&mut Transform, &SpriteChild), With<Ghost>>,
    mut replay_state: ResMut<Replaying>,
    mut sprites: Query<&mut Sprite, Without<KeyboardControlled>>,
    time: Res<Time>,
    mut blast_spawner: EventWriter<SpawnBlast>,
    mut next_state: ResMut<NextState<PlayerState>>,
) {
    for (mut transform, sprite_ptr) in ghost.iter_mut() {
        replay_state.0.timer.tick(time.delta());
        let elapsed = replay_state.0.timer.elapsed_secs();

        let Ok(mut sprite) = sprites.get_mut(sprite_ptr.0) else {
            continue;
        };

        while let Some(next_item) = replay_state.0.state_stack.pop_front() {
            if next_item.0 >= elapsed {
                replay_state.0.state_stack.push_front(next_item);
                break;
            }

            let (x, y) = (
                next_item.1.character.location.x,
                next_item.1.character.location.y,
            );

            transform.translation.x = x;
            transform.translation.y = y;

            sprite.flip_x = next_item.1.character.flip_x;

            sprite.texture_atlas.alter(|atlas| {
                atlas.index = next_item.1.character.atlas_index;
            });

            if next_item.1.blasting {
                blast_spawner.send(SpawnBlast {
                    location: next_item.1.character.location,
                });
            }
        }

        if replay_state.0.state_stack.len() == 0 {
            next_state.set(PlayerState::Combined);
        }
    }
}

pub fn toggle_player_character(
    keys: Res<ButtonInput<KeyCode>>,
    state: Res<State<PlayerState>>,
    mut next_state: ResMut<NextState<PlayerState>>,
) {
    if keys.just_pressed(KeyCode::KeyZ) {
        next_state.set(match state.get() {
            PlayerState::Combined => PlayerState::Ghost,
            PlayerState::Ghost => PlayerState::Human,
            PlayerState::Human => return,
        });
    }
}

pub fn switch_to_combined(
    human: Query<(Entity, &Transform, &SpriteChild, &Health), With<Human>>,
    ghost: Query<Entity, With<Ghost>>,
    mut despawn: EventWriter<Despawn>,
    mut character_spawner: EventWriter<SpawnCharacter>,
    sprites: Query<&Sprite>,
) {
    let Ok((human, transform, sprite_ptr, health)) = human.get_single() else {
        return;
    };

    let Ok(ghost) = ghost.get_single() else {
        return;
    };

    let location = transform.translation.truncate();

    let Ok(human_sprite) = sprites.get(sprite_ptr.0) else {
        return;
    };

    despawn.send(Despawn(human));
    despawn.send(Despawn(ghost));

    character_spawner.send(SpawnCharacter {
        location,
        controlled: true,
        character_type: CharacterType::Combined,
        flip_x: human_sprite.flip_x,
        health: health.current,
        ..default()
    });
}

pub fn switch_to_human(
    mut commands: Commands,
    ghost: Query<(Entity, &SpriteChild, &Health), With<Ghost>>,
    human: Query<Entity, With<Human>>,
    mut despawn: EventWriter<Despawn>,
    mut character_spawner: EventWriter<SpawnCharacter>,
    sprites: Query<&Sprite>,
    record_state: Res<Recording>,
) {
    let Ok((ghost, sprite_ptr, health)) = ghost.get_single() else {
        return;
    };

    let Ok(sprite) = sprites.get(sprite_ptr.0) else {
        return;
    };

    despawn.send(Despawn(ghost));
    let Ok(human) = human.get_single() else {
        return;
    };
    despawn.send(Despawn(human));

    commands.remove_resource::<Recording>();
    commands.insert_resource(Replaying(RecordState {
        timer: Stopwatch::default(),
        state_stack: record_state.0.state_stack.clone(),
    }));

    let ghost_start = &record_state.0.state_stack[0].1.character;

    character_spawner.send(SpawnCharacter {
        location: ghost_start.location,
        controlled: true,
        character_type: CharacterType::Human,
        flip_x: sprite.flip_x,
        health: health.current,
        ..default()
    });

    character_spawner.send(SpawnCharacter {
        location: ghost_start.location,
        controlled: false,
        character_type: CharacterType::Ghost,
        flip_x: sprite.flip_x,
        physics: false,
        health: health.current,
        ..default()
    });
}

const GHOST_TIMER: Duration = Duration::from_secs(5);

pub fn ghost_timer(record: Res<Recording>, mut next_state: ResMut<NextState<PlayerState>>) {
    if record.0.timer.elapsed() > GHOST_TIMER {
        next_state.set(PlayerState::Human);
    }
}

#[derive(Component)]
pub struct RecordTimerUi;

pub fn spawn_record_timer_ui(mut commands: Commands) {
    commands
        .spawn(Node {
            flex_direction: FlexDirection::Column,
            justify_content: JustifyContent::End,
            align_items: AlignItems::End,
            height: Val::Vh(100.0),
            width: Val::Vw(100.0),
            padding: UiRect::all(Val::Px(24.0)),
            ..default()
        })
        .with_child((
            Text::default(),
            TextFont::from_font_size(64.0),
            RecordTimerUi,
        ));
}

pub fn despawn_record_timer_ui(
    ui: Query<Entity, With<RecordTimerUi>>,
    mut despawner: EventWriter<Despawn>,
) {
    if let Ok(entity) = ui.get_single() {
        despawner.send(Despawn(entity));
    }
}

pub fn update_record_timer_ui(
    record: Res<Recording>,
    mut text: Query<&mut Text, With<RecordTimerUi>>,
) {
    let Ok(mut text) = text.get_single_mut() else {
        return;
    };

    let seconds_left =
        (GHOST_TIMER.as_secs_f64() - record.0.timer.elapsed_secs_f64()).ceil() as u32;

    *text = Text::new(seconds_left.to_string());
}

pub fn switch_to_ghost(
    mut commands: Commands,
    character: Query<(Entity, &Transform, &SpriteChild, &Health), With<KeyboardControlled>>,
    mut despawn: EventWriter<Despawn>,
    mut character_spawner: EventWriter<SpawnCharacter>,
    sprites: Query<&Sprite>,
) {
    let Ok((entity, transform, sprite_ptr, health)) = character.get_single() else {
        return;
    };

    let Ok(sprite) = sprites.get(sprite_ptr.0) else {
        return;
    };

    let Some(atlas) = &sprite.texture_atlas else {
        return;
    };

    despawn.send(Despawn(entity));

    let location = transform.translation.truncate();

    commands.insert_resource(Recording(RecordState {
        timer: Stopwatch::new(),
        state_stack: VecDeque::from([(
            0.0,
            RecordedInstant {
                character: CharacterLocationAndAnimation {
                    location,
                    atlas_index: atlas.index,
                    flip_x: sprite.flip_x,
                },
                blasting: false,
            },
        )]),
    }));

    character_spawner.send(SpawnCharacter {
        location,
        controlled: true,
        character_type: CharacterType::Ghost,
        flip_x: sprite.flip_x,
        health: health.current,
        ..default()
    });

    character_spawner.send(SpawnCharacter {
        location,
        controlled: false,
        character_type: CharacterType::Human,
        flip_x: sprite.flip_x,
        health: health.current,
        ..default()
    });
}

pub fn pause(
    mut commands: Commands,
    should_pause: Query<
        (
            Entity,
            &SpriteChild,
            Option<&KeyboardControlled>,
            Option<&LinearVelocity>,
        ),
        With<ShouldPause>,
    >,
    mut animations: Query<&mut CharacterAnimationTracker>,
) {
    for (entity, sprite, controlled, velocity) in should_pause.iter() {
        let mut entity = commands.entity(entity);

        if controlled.is_none() {
            entity.insert(RigidBody::Static);

            if let Some(velocity) = velocity {
                entity.insert(ResumeState {
                    velocity: velocity.0,
                });
            }

            if let Ok(mut tracker) = animations.get_mut(sprite.0) {
                tracker.paused = true;
            };
        }
    }
}

pub fn unpause(
    mut commands: Commands,
    should_pause: Query<(Entity, &SpriteChild, Option<&ResumeState>), With<ShouldPause>>,
    mut animations: Query<&mut CharacterAnimationTracker>,
) {
    for (entity, sprite, resume_state) in should_pause.iter() {
        let mut entity = commands.entity(entity);

        entity.insert(RigidBody::Dynamic);
        if let Ok(mut tracker) = animations.get_mut(sprite.0) {
            tracker.paused = false;
        };

        if let Some(state) = resume_state {
            entity.remove::<ResumeState>();
            entity.insert(LinearVelocity::from(state.velocity));
        }
    }
}
