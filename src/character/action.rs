use bevy::prelude::*;

use crate::inventory::Inventory;
use crate::projectiles::{ProjectileDirection, SpawnProjectile};
use crate::{Despawn, SpriteChild};

use super::movement::KeyboardControlled;
use super::switching::{PlayerState, RecordBlast};

#[derive(SubStates, Clone, PartialEq, Eq, Hash, Debug, Default)]
#[source(PlayerState = PlayerState::Combined)]
pub enum Reloading {
    #[default]
    No,
    Yes,
}

#[derive(Component)]
pub struct ReloadTimer(pub Timer);

pub fn handle_reload_timer(
    mut reload_timer: Query<(&mut ReloadTimer, Entity)>,
    mut inventory: ResMut<Inventory>,
    time: Res<Time>,
    mut despawner: EventWriter<Despawn>,
    mut reload_state: ResMut<NextState<Reloading>>,
) {
    let Ok((mut reload_timer, entity)) = reload_timer.get_single_mut() else {
        return;
    };

    reload_timer.0.tick(time.delta());
    if reload_timer.0.just_finished() {
        inventory.reload();
        despawner.send(Despawn(entity));
        reload_state.set(Reloading::No);
    }
}

pub fn player_action(
    mut commands: Commands,
    keys: Res<ButtonInput<KeyCode>>,
    mouse: Res<ButtonInput<MouseButton>>,
    mut controlled: Query<(&Transform, &SpriteChild, Entity), With<KeyboardControlled>>,
    mut projectile_spawner: EventWriter<SpawnProjectile>,
    mut blast_recorder: EventWriter<RecordBlast>,
    mut inventory: ResMut<Inventory>,
    player_state: Res<State<PlayerState>>,
    mut reload_state: ResMut<NextState<Reloading>>,
    sprites: Query<&Sprite>,
) {
    let Ok((transform, sprite_ptr, entity)) = controlled.get_single_mut() else {
        return;
    };

    let Ok(sprite) = sprites.get(sprite_ptr.0) else {
        return;
    };

    if let PlayerState::Combined = player_state.get() {
        if keys.just_pressed(KeyCode::KeyR) && inventory.ammunition < inventory.max_ammunition {
            reload_state.set(Reloading::Yes);
            commands.spawn(ReloadTimer(Timer::from_seconds(3.0, TimerMode::Once)));
        }
    }

    if mouse.just_pressed(MouseButton::Left) || keys.just_pressed(KeyCode::KeyF) {
        let location = transform.translation.truncate();

        if *player_state.get() == PlayerState::Ghost {
            blast_recorder.send(RecordBlast);
        } else if inventory.ammunition > 0 {
            inventory.ammunition -= 1;

            projectile_spawner.send(SpawnProjectile {
                location,
                flip_x: sprite.flip_x,
                speed: player_state.get().bullet_speed(),
                damage: 10.0,
                shooter: entity,
                direction: ProjectileDirection::Cursor,
            });
        }
    }
}
