use avian2d::prelude::*;
use bevy::prelude::*;

const DEFAULT_GRAVITY: f32 = 100.0;

#[derive(Default, PhysicsLayer)]
pub(crate) enum Layer {
    #[default]
    Level,
    Character,
    Projectile,
    Box,
}

impl Layer {
    pub(crate) fn level() -> CollisionLayers {
        CollisionLayers::new(Self::Level, LayerMask::ALL)
    }

    pub(crate) fn character() -> CollisionLayers {
        CollisionLayers::new(Self::Character, [Self::Level, Self::Box])
    }

    pub(crate) fn projectile() -> CollisionLayers {
        CollisionLayers::new(Self::Projectile, [Self::Projectile])
    }
}

pub(crate) struct PhysicsPlugin {
    pub(crate) gravity: f32,
}

impl Default for PhysicsPlugin {
    fn default() -> Self {
        Self {
            gravity: DEFAULT_GRAVITY,
        }
    }
}

impl Plugin for PhysicsPlugin {
    fn build(&self, app: &mut App) {
        app.add_plugins(PhysicsPlugins::default())
            .insert_resource(Gravity(Vec2::NEG_Y * self.gravity));
    }
}
