use std::time::Duration;

use bevy::prelude::*;
use bevy_tnua::{builtins::TnuaBuiltinWalk, controller::TnuaController};

use crate::{
    character::{movement::KeyboardControlled, switching::PlayerState},
    game_state::InGame,
    projectiles::{ProjectileDirection, SpawnProjectile},
    SpriteChild,
};

#[derive(Component)]
pub struct Npc;

#[derive(Default)]
pub struct NpcPlugin;

#[derive(Component, Clone)]
pub struct BehaviourConfig(pub Vec<Behaviour>);

impl Default for BehaviourConfig {
    fn default() -> Self {
        Self(vec![Behaviour::default()])
    }
}

#[derive(Default, Clone)]
pub enum Behaviour {
    #[default]
    IdleForever,
    FollowPlayer {
        activation_distance: f32,
        target_distance: f32,
        speed: f32,
    },
    ShootInRange {
        range: f32,
        attack_speed: Duration,
        projectile_speed: f32,
        projectile_damage: f32,
        timer: Timer,
    },
}

#[derive(Component, Clone, Default)]
enum MovementConfig {
    #[default]
    Idle,
    MoveToPlayer {
        speed: f32,
    },
}

fn handle_behaviour(
    mut commands: Commands,
    player: Query<&Transform, With<KeyboardControlled>>,
    mut npcs: Query<(Entity, &mut BehaviourConfig, &Transform), With<Npc>>,
    time: Res<Time>,
    mut projectile_spawner: EventWriter<SpawnProjectile>,
) {
    let Ok(player) = player.get_single() else {
        return;
    };

    for (npc, mut behaviours, location) in npcs.iter_mut() {
        let distance = location.translation.distance(player.translation);

        for behaviour in behaviours.0.iter_mut() {
            match behaviour {
                Behaviour::IdleForever => {
                    commands.entity(npc).insert(MovementConfig::Idle);
                }
                Behaviour::FollowPlayer {
                    activation_distance,
                    target_distance,
                    speed,
                } => {
                    let should_move =
                        distance < *activation_distance && distance > *target_distance;
                    commands.entity(npc).insert(if should_move {
                        MovementConfig::MoveToPlayer { speed: *speed }
                    } else {
                        MovementConfig::Idle
                    });
                }
                Behaviour::ShootInRange {
                    range,
                    attack_speed,
                    projectile_speed,
                    projectile_damage,
                    timer,
                } => {
                    timer.tick(time.delta());
                    if distance < *range && timer.finished() {
                        projectile_spawner.send(SpawnProjectile {
                            location: location.translation.truncate(),
                            flip_x: true,
                            speed: *projectile_speed,
                            damage: *projectile_damage,
                            shooter: npc,
                            direction: ProjectileDirection::Target(player.translation.truncate()),
                        });
                        *timer = Timer::new(*attack_speed, TimerMode::Once);
                    }
                }
            }
        }
    }
}

fn handle_movement(
    player: Query<&Transform, With<KeyboardControlled>>,
    mut npcs: Query<
        (
            &mut TnuaController,
            &Transform,
            &MovementConfig,
            &SpriteChild,
        ),
        With<Npc>,
    >,
    mut sprites: Query<&mut Sprite>,
) {
    let Ok(player) = player.get_single() else {
        return;
    };

    for (mut npc, location, movement, sprite_ptr) in npcs.iter_mut() {
        let Ok(mut sprite) = sprites.get_mut(sprite_ptr.0) else {
            continue;
        };

        match movement {
            MovementConfig::Idle => {
                npc.basis(TnuaBuiltinWalk {
                    desired_velocity: Vec3::ZERO,
                    desired_forward: Some(Dir3::X),
                    float_height: 18.2,
                    ..default()
                });
            }
            MovementConfig::MoveToPlayer { speed } => {
                let direction = (player.translation - location.translation).normalize();
                npc.basis(TnuaBuiltinWalk {
                    desired_velocity: direction * *speed,
                    desired_forward: Dir3::new(direction).ok(),
                    float_height: 18.2,
                    acceleration: 1000.0,
                    ..default()
                });

                if direction.x > 0.0 {
                    sprite.flip_x = false;
                } else if direction.x < 0.0 {
                    sprite.flip_x = true;
                }
            }
        }
    }
}

impl Plugin for NpcPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(
            Update,
            (handle_behaviour, handle_movement)
                .run_if(in_state(InGame))
                .run_if(not(in_state(PlayerState::Ghost))),
        );
    }
}
