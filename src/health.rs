use crate::{game_state::InGame, z::ZIndex};
use bevy::prelude::*;

#[derive(Copy, Clone, Component, Debug)]
pub struct Health {
    pub max: f32,
    pub current: f32,
    pub bar: Entity,
}

#[derive(Component)]
#[require(Text2d)]
pub struct HealthBar;

impl HealthBar {
    pub fn spawn<'a>(max: f32, commands: &'a mut Commands) -> EntityCommands<'a> {
        commands.spawn((
            HealthBar,
            Text2d::new(format!("{}", max)),
            TextColor::WHITE,
            TextFont::from_font_size(50.0),
            Transform {
                translation: Vec3::new(0.0, 20.0, ZIndex::Ui.z()),
                scale: Vec2::splat(0.2).extend(1.0),
                ..default()
            },
        ))
    }
}

#[derive(Default)]
pub struct HealthPlugin;

fn sync_health_bar(health: Query<&Health>, mut bars: Query<&mut Text2d, With<HealthBar>>) {
    for health in health.iter() {
        let Ok(mut bar) = bars.get_mut(health.bar) else {
            continue;
        };

        *bar = Text2d::new(health.current.to_string());
    }
}

impl Plugin for HealthPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(Update, sync_health_bar.run_if(in_state(InGame)));
    }
}
