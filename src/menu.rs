use bevy::prelude::*;

use crate::{game_state::MainGameState, levels::Level, Despawn};

#[derive(Default)]
pub(crate) struct MenuPlugin;

impl Plugin for MenuPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(OnEnter(MainGameState::MainMenu), spawn_menu)
            .add_systems(OnExit(MainGameState::MainMenu), despawn_menu)
            .add_systems(
                Update,
                (
                    load_on_enter.run_if(in_state(MainGameState::MainMenu)),
                    back_to_menu.run_if(not(in_state(MainGameState::MainMenu))),
                ),
            );
    }
}

#[derive(Component)]
struct MainMenu;

fn spawn_menu(mut commands: Commands) {
    commands
        .spawn((Node::default(), MainMenu))
        .with_children(|parent| {
            parent.spawn((
                Text::new("Enter to start, escape in-game to return to menu."),
                TextFont::from_font_size(40.0),
            ));
        });
}

fn despawn_menu(menu: Query<Entity, With<MainMenu>>, mut despawner: EventWriter<Despawn>) {
    for menu in menu.iter() {
        despawner.send(Despawn(menu));
    }
}

fn load_on_enter(keys: Res<ButtonInput<KeyCode>>, mut next: ResMut<NextState<MainGameState>>) {
    if keys.just_pressed(KeyCode::Enter) {
        next.set(MainGameState::Loading(Level::Level1));
    }
}

fn back_to_menu(keys: Res<ButtonInput<KeyCode>>, mut next: ResMut<NextState<MainGameState>>) {
    if keys.just_pressed(KeyCode::Escape) {
        next.set(MainGameState::MainMenu);
    }
}
